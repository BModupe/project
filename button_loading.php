<!DOCTYPE html>
<html>
<head>
	<title>yeah</title>

	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<button id="dButton" onclick="change();">
	  	Button
	</button>
	<button id="buttonload" style="display: none;">
	  	<i class="fa fa-spinner fa-spin"></i> Loading... Please wait
	</button>

	<script type="text/javascript">
		function change(){
			document.getElementById('dButton').style.display = 'none';
			document.getElementById('buttonload').style.display = 'block';
		}
	</script>
</body>
</html>