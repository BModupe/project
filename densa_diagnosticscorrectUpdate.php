<?php
$conn = mysqli_connect("localhost", "root", "", "hospital_diagnostics");

if(!$conn){
	die("Connection failed: ".mysqli_connect_error());
}		


/*if (isset($_POST['initials'])){
	$initials = $_POST['initials'];
	$age = $_POST['age'];
	$sex = $_POST['sex'];
	$hospital_no = $_POST['hospital_no'];
	$department = $_POST['department'];
	$state = $_POST['state'];
	$local_government = $_POST['local_government'];
	$diagnosis = $_POST['diagnosis'];
	$users_symptoms = $_POST['users_symptoms'];
	$type_of_diagnosis = $_POST['type_of_diagnosis'];
	$type_of_healthFacility = $_POST['type_of_healthFacility'];
	$level_of_healthFacility = $_POST['level_of_healthFacility'];

	$sql = "INSERT INTO disease_diagnostics_table (initials, age, sex, hospital_no, department, state, local_government, diagnosis, users_symptoms, type_of_diagnosis, type_of_healthFacility, level_of_healthFacility) VALUES ('$initials', '$age', '$sex', '$hospital_no', '$department', '$state', '$local_government', '$diagnosis', '$users_symptoms', '$type_of_diagnosis', '$type_of_healthFacility', '$level_of_healthFacility')";

	$result = $conn->query($sql);
	echo 'Successful';
}
else {
	echo "Failed";
}*/


/****--- SHOWING DATABASE OUTPUT ---***/

function getDiagnostics($conn){
	$sql2 = "SELECT * FROM disease_diagnostics_table";
	$result2 = $conn->query($sql2);

	echo "<table>
	 	<tr>
	 		<th>ID</th>
	 		<th>INITIALS</th>
	 		<th>AGE</th>
	 		<th>SEX</th>
	 		<th>HOSPITAL NUMBER</th>
	 		<th>DEPARTMENT</th>
	 		<th>STATE</th>
	 		<th>LOCAL GOVERNMENT AREA</th>
	 		<th>DIAGNOSIS</th>
	 		<th>USERS SYMPTOMS</th>
	 		<th>TYPE OF DIAGNOSIS</th>
	 		<th>TYPE OF HEALTH FACILITY</th>
	 		<th>LEVEL OF HEALTH FACILITY</th>

	 	</tr>";
	while ($row = $result2->fetch_assoc()) { //loops through all the result and fetches the result
		$id = $row['initials'];
		echo "<tr>
		 		<td>".$row['id']."</td>
		 		<td>".$row['initials']."</td>
		 		<td>".$row['age']."</td>
		 		<td>".$row['sex']."</td>
		 		<td>".$row['hospital_no']."</td>
		 		<td>".$row['department']."</td>
		 		<td>".$row['state']."</td>
		 		<td>".$row['local_government']."</td>
		 		<td>".$row['diagnosis']."</td>
		 		<td>".$row['users_symptoms']."</td>
		 		<td>".$row['type_of_diagnosis']."</td>
		 		<td>".$row['type_of_healthFacility']."</td>
		 		<td>".$row['level_of_healthFacility']."</td>
		 	</tr>";	
	}
	echo "</table>";

}



/*******++--- CALCULATING PERCENTAGE ---++*******/


/*** SEX ***/
$sex_sql1 = "SELECT * FROM disease_diagnostics_table WHERE (`sex` LIKE 'M%')";
$sex_sql2 = "SELECT * FROM disease_diagnostics_table WHERE (`sex` LIKE 'F%')";
$sex_sql3 = "SELECT sex FROM disease_diagnostics_table";

$sex_result1 = $conn->query($sex_sql1);
$sex_rowcount1 = $sex_result1->num_rows;

$sex_result2 = $conn->query($sex_sql2);
$sex_rowcount2 = $sex_result2->num_rows;

$sex_result3 = $conn->query($sex_sql3);
$sex_rowcount3 = $sex_result3->num_rows;


$sex_output1 = ($sex_rowcount1/$sex_rowcount3)*100;
//echo 'Percentage of Male = '.$sex_output1.'%<br>';
$sex_output2 = ($sex_rowcount2/$sex_rowcount3)*100;
//echo 'Percentage of Female = '.$sex_output2.'%<br>';

/*** DEPARTMENT ***/

$dep_sql1 = "SELECT * FROM disease_diagnostics_table WHERE (`department` LIKE 'R%')";
$dep_sql2 = "SELECT * FROM disease_diagnostics_table WHERE (`department` LIKE 'C%')";
$dep_sql3 = "SELECT department FROM disease_diagnostics_table";

$dep_result1 = $conn->query($dep_sql1);
$dep_rowcount1 = $dep_result1->num_rows;

$dep_result2 = $conn->query($dep_sql2);
$dep_rowcount2 = $dep_result2->num_rows;

$dep_result3 = $conn->query($dep_sql3);
$dep_rowcount3 = $dep_result3->num_rows;


$dep_output1 = ($dep_rowcount1/$dep_rowcount3)*100;
//echo 'Percentage of --- = '.$dep_output1.'%<br>';
$dep_output2 = ($dep_rowcount2/$dep_rowcount3)*100;
//echo 'Percentage of --- = '.$dep_output2.'%<br>';

/*** DIAGNOSIS ***/

$dgn_sql1 = "SELECT * FROM disease_diagnostics_table WHERE (`diagnosis` LIKE 'E%')";
$dgn_sql2 = "SELECT * FROM disease_diagnostics_table WHERE (`diagnosis` LIKE 'C%')";
$dgn_sql3 = "SELECT diagnosis FROM disease_diagnostics_table";

$dgn_result1 = $conn->query($dgn_sql1);
$dgn_rowcount1 = $dgn_result1->num_rows;

$dgn_result2 = $conn->query($dgn_sql2);
$dgn_rowcount2 = $dgn_result2->num_rows;

$dgn_result3 = $conn->query($dgn_sql3);
$dgn_rowcount3 = $dgn_result3->num_rows;


$dgn_output1 = ($dgn_rowcount1/$dgn_rowcount3)*100;
//echo 'Percentage of --- = '.$dgn_output1.'%<br>';
$dgn_output2 = ($dgn_rowcount2/$dgn_rowcount3)*100;
//echo 'Percentage of --- = '.$dgn_output2.'%';